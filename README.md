![思飞工作室](http://thinkfly.qiniudn.com/logo.png)
#Template4STM32F407
By ```Nick.liao @ HUST-MSE-ThinkFly.Studio 2014-10-25(星期六) ```
##介绍
这是一个提供```STM32F407```的工程模板，移植操作系统```μC/OS-III```  
我们将：  

1. 完善```μC/OS-III```的移植，开启FPU等
2. 提供常用外设的板级支持包BSP
3. 提供IAP或者调用系统bootloader升级

##硬件  

- STM32F407ZG

##编译器
###```MDK keil5.1```

##系统
### ```uCOS-III```

##文件路径说明
- APP 下面存放直接和工程相关文件，比如main函数文件，应用配置文件等
- BSP下面有INC、SRC两个文件夹，用于存放板级支持包文件，INC存放头文件、SRC存放c源文件
- PROJECT文件夹存放工程文件和库函数文件，**不需要修改**
- OBJ文件夹用于存放编译中间文件，需要建立
- uCOS文件夹存放关于```μC/OS-III```的源文件
- USERLIB文件夹存放与硬件无关的用户模块，INC、SRC同样分头文件和源文件存放

##板级支持包BSP
- bsp_led  ------提供各部分led支持

##使用
1. 在APP的同级创建OBJ文件夹，用于存放编译中间文件
2. 打开PROJECT下面的APP.uvprojx工程文件即可

##升级改动
查看APP/LOG.txt即可，每一次的修改均会在此记录
