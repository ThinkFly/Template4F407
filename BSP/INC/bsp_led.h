/**
  ******************************************************************************
  * @file    bsp_led.h
  * @author  Nick Thinkfly.studio
  * @version V1.0
  * @date    2014-10-6
  * @brief   This file contains all the functions prototypes for the BOARD
  *  bsp_led
  ******************************************************************************
  * @attention
  *
  * FILE FOR THINKFLY.STUDIO ONLY
  *
  * <h2><center>&copy; COPYRIGHT ThinkFly.Studio</center></h2>
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __BSP_LED_H
#define __BSP_LED_H

/* Includes ------------------------------------------------------------------*/

#include "bsp.h"


/* Exported typedef ----------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/

#define GPIOE_ODR_Addr    (GPIOE_BASE+0x14)
#define GPIOE2      0x42000000+(GPIOE_ODR_Addr-0x40000000)*32+2*4
#define GPIOE3      0x42000000+(GPIOE_ODR_Addr-0x40000000)*32+3*4
#define LEDiR        *((volatile unsigned long *) (GPIOE2))
#define LEDiG        *((volatile unsigned long *) (GPIOE3))


/* Exported functions ------------------------------------------------------- */

void BSP_LED_Init ( void );
void BSP_LED_Off ( void );

#endif  /* __LED_H */

