/**
  ******************************************************************************
  * @file    app.c
  * @author  Nick Thinkfly.studio
  * @version V1.1
  * @date    2014-10-6
  * @brief   main function
  ******************************************************************************
  * @attention
  *
  * FILE FOR THINKFLY.STUDIO ONLY
  *
  * <h2><center>&copy; COPYRIGHT ThinkFly.Studio</center></h2>
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/

#include <app_cfg.h>
#include <includes.h>

/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

static  OS_TCB   AppTaskStartTCB;
static  CPU_STK  AppTaskStartStk[APP_CFG_TASK_START_STK_SIZE];

/* Private function prototypes -----------------------------------------------*/

static  void  AppTaskStart          ( void     *p_arg );
static  void  AppTaskCreate         ( void );

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  app入口
  * @note   None
  * @param  void
  * @retval None
  * @author Nick.liao
  */
int main ( void )
{
    OS_ERR  err;

    OSInit ( &err );                                            /* Init uC/OS-III.                                      */

    OSTaskCreate ( ( OS_TCB       * ) &AppTaskStartTCB,         /* Create the start task                                */
                   ( CPU_CHAR     * ) "App Task Start",
                   ( OS_TASK_PTR   ) AppTaskStart,
                   ( void         * ) 0,
                   ( OS_PRIO       ) APP_CFG_TASK_START_PRIO,
                   ( CPU_STK      * ) &AppTaskStartStk[0],
                   ( CPU_STK_SIZE  ) AppTaskStartStk[APP_CFG_TASK_START_STK_SIZE / 10],
                   ( CPU_STK_SIZE  ) APP_CFG_TASK_START_STK_SIZE,
                   ( OS_MSG_QTY    ) 0,
                   ( OS_TICK       ) 0,
                   ( void         * ) 0,
                   ( OS_OPT        ) ( OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR ),
                   ( OS_ERR       * ) &err );

    OSStart ( &err );                                           /* Start multitasking (i.e. give control to uC/OS-III). */

    ( void ) &err;

    return ( 0 );
}


/**
  * @brief  开启多任务
  * @note   None
  * @param  p_arg 创建任务传递的参数
  * @retval None
  * @author Nick
  */
static  void  AppTaskStart ( void *p_arg )
{
    OS_ERR      err;


    ( void ) p_arg;

    BSP_Init();                                                 /* Initialize BSP functions                             */
    CPU_Init();                                                 /* Initialize the uC/CPU services                       */

    BSP_Tick_Init();                                            /* Start Tick Initialization                            */

//    Mem_Init();                                                 /* Initialize Memory Management Module                  */
//    Math_Init();                                                /* Initialize Mathematical Module                       */


#if OS_CFG_STAT_TASK_EN > 0u
    OSStatTaskCPUUsageInit ( &err );                            /* Compute CPU capacity with no task running            */
#endif

#ifdef CPU_CFG_INT_DIS_MEAS_EN
    CPU_IntDisMeasMaxCurReset();
#endif

    BSP_LED_Off();

#if (APP_CFG_SERIAL_EN == DEF_ENABLED)
    App_SerialInit();                                           /* Initialize Serial Communication                      */
#endif

    AppTaskCreate();                                            /* Create Application tasks                             */

    LEDiG = 1;
    while ( DEF_TRUE )                                          /* Task body, always written as an infinite loop.       */
    {
        LEDiG = !LEDiG;
        LEDiR = !LEDiR;
        OSTimeDlyHMSM ( 0, 0, 0, 300,
                        OS_OPT_TIME_HMSM_STRICT,
                        &err );
    }
}

/**
  * @brief  创建任务
  * @note   None
  * @param  void
  * @retval None
  * @author Nick
  */
static  void  AppTaskCreate         ( void )
{

}



#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed ( uint8_t* file, uint32_t line )
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
    /* Infinite loop */
    while ( 1 )
    {

    }
}
#endif




